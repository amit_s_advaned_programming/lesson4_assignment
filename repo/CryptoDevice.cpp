#include "CryptoDevice.h"
#include "pch.h"
#include "base64.h"
#include <filters.h>
#include "rsa.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];


CryptoDevice::CryptoDevice(){}
CryptoDevice::~CryptoDevice(){}


string CryptoDevice::encryptAES(string plainText)
{

	string cipherText;
	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();
	return cipherText;
}

string CryptoDevice::decryptAES(string cipherText)
{

	string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}


string CryptoDevice::decryptRSA(string cipherText, CryptoPP::RSA::PrivateKey key, CryptoPP::AutoSeededRandomPool& rng)
{
	string decryptedText;

	CryptoPP::RSAES_OAEP_SHA_Decryptor d(key);
	CryptoPP::StringSource ss2(cipherText, true,
		new CryptoPP::PK_DecryptorFilter(rng, d,
		new CryptoPP::StringSink(decryptedText)
		) // PK_DecryptorFilter
		); // StringSource


	return decryptedText;


}

string CryptoDevice::encryptRSA(string cipherText, CryptoPP::RSA::PublicKey key, CryptoPP::AutoSeededRandomPool& rng)
{
	string encryptedText;

	CryptoPP::RSAES_OAEP_SHA_Encryptor e(key);

	CryptoPP::StringSource ss1(cipherText, true,
		new CryptoPP::PK_EncryptorFilter(rng, e,
		new CryptoPP::StringSink(encryptedText)
		) // PK_EncryptorFilter
		); // StringSource


	return encryptedText;
}

