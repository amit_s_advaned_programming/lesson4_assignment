#pragma once

#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include "rsa.h"
#include <cstdlib>

using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string encryptAES(string);
	string decryptAES(string);
	string encryptRSA(string cipherText, CryptoPP::RSA::PublicKey key, CryptoPP::AutoSeededRandomPool& rng);
	string decryptRSA(string cipherText, CryptoPP::RSA::PrivateKey key, CryptoPP::AutoSeededRandomPool& rng);


};
